const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк"
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка"
    }
];

const elemToAttach = document.querySelector("#root");

class List {
    constructor(type, domElemToAdd) {
        this.type = type;
        this.domElemToAdd = domElemToAdd;
        this.list = this.render()
    }

    render() {
        return document.createElement(this.type);
    }

    attach() {
        this.domElemToAdd.prepend(this.list);
    }

    addElem(elem) {
        this.list.innerHTML += elem;
    }
}

class BookValidationError extends Error {
    constructor(message) {
        super(message);
        this.name = "BookValidationError";
    }
}

class BookParser {
    constructor(bookList) {
        this.bookList = bookList;
    }

    parse() {
        if (!Array.isArray(this.bookList)) {
            console.error("First argument must be an Array");
        } else if (this.bookList.length === 0) {
            console.error("Given Array length is 0");
        } else {
            const elemsList = new List("ul", elemToAttach);
            elemsList.attach();
            for (const bookInfo of this.bookList) {
                try {
                    if (!bookInfo.author) {
                        throw new BookValidationError("No property - author in books object");
                    } else if (!bookInfo.name) {
                        throw new BookValidationError("No property - name in books object");
                    } else if (!bookInfo.price) {
                        throw new BookValidationError("No property - price in books object");
                    } else {
                        elemsList.addElem(`<li>Author: ${bookInfo.author}, Book name: ${bookInfo.name}, Price: ${bookInfo.price}</li>`)
                    }
                } catch (e) {
                    console.error(e.stack);
                }
            }
        }
    }
}

const store = new BookParser(books);
store.parse();