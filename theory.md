Конструкцию try...catch уместно использовать в случае, если мы хотим скрыть от пользователя ошибку выполнения скрипта,
или хотим отловить возможную ошибку в коде, при этом не дать скрипту завалится.
Также возможно использование конструкции try...catch при генерации собственных собственных ошибок и их отлавливания, 
с целью логирования, или проведения дальнейших действий в коде.

